package ru.otus.example.ormbasicsdemo.repositories.mybatis;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import ru.otus.example.ormbasicsdemo.models.jdbc.EMail;

import java.util.List;

@Mapper
public interface EmailRepositoryMyBatis {

    @Select("select * from emails where student_id = #{studentId}")
    List<EMail> getEmailsByStudentId(long studentId);
}

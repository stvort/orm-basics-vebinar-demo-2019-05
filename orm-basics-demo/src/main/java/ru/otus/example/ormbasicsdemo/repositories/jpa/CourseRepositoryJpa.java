package ru.otus.example.ormbasicsdemo.repositories.jpa;

import ru.otus.example.ormbasicsdemo.models.jpa.common.Course;

public interface CourseRepositoryJpa {
    Course save(Course course);
}

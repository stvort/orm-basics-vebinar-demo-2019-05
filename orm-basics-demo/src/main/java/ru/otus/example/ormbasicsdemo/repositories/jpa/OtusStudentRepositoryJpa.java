package ru.otus.example.ormbasicsdemo.repositories.jpa;


import ru.otus.example.ormbasicsdemo.models.jpa.OtusStudent;

import java.util.List;
import java.util.Optional;

public interface OtusStudentRepositoryJpa {
    Optional<OtusStudent> findById(long id);
    List<OtusStudent> findAll();
    OtusStudent save(OtusStudent student);

}

package ru.otus.example.ormbasicsdemo.repositories.jdbc;

import ru.otus.example.ormbasicsdemo.models.jdbc.Course;

import java.util.List;

public interface CourseRepositoryJdbc {
    List<Course> findAllUsed();
}

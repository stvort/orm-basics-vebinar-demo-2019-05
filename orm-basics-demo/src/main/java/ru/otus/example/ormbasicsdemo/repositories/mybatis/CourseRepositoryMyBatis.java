package ru.otus.example.ormbasicsdemo.repositories.mybatis;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import ru.otus.example.ormbasicsdemo.models.jdbc.Course;

import java.util.List;

@Mapper
public interface CourseRepositoryMyBatis {

    @Select("select * from student_courses sc left join courses c on sc.course_id = c.id where sc.student_id = #{studentId}")
    List<Course> getCoursesByStudentId(long studentId);

}

package ru.otus.example.ormbasicsdemo.repositories.jdbc;

import ru.otus.example.ormbasicsdemo.models.jdbc.OtusStudent;

import java.util.List;

public interface OtusStudentRepositoryJdbc {
    List<OtusStudent> findAllWithAllInfo();
}

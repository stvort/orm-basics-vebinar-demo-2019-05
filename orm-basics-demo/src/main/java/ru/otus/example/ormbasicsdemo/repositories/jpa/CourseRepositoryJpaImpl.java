package ru.otus.example.ormbasicsdemo.repositories.jpa;

import ru.otus.example.ormbasicsdemo.models.jpa.common.Course;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class CourseRepositoryJpaImpl implements CourseRepositoryJpa {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Course save(Course course) {
        em.persist(course);
        return course;
    }
}

package ru.otus.example.ormbasicsdemo.repositories.jpa;


import ru.otus.example.ormbasicsdemo.models.jpa.OtusStudentV2;

import java.util.List;

public interface OtusStudentV2RepositoryJpa {
    List<OtusStudentV2> findAllWithEntityGraph();
    List<OtusStudentV2> findAllWithJoinFetch();
}
